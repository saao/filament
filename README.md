# Filament

A toolkit for building web-based user interfaces at the South African
Astronomical Observatory.


## Getting Started

Building a new instrument or telescope control user interface should ideally be
as easy as installing a single package. The steps below describe how to get
started in detail.


### Create an OAuth application

Your application will make use of the observer portal for authentication.
Create a new OAuth application by going to:

    https://portal.saao.ac.za/o/applications/

You'll need the client ID and secret for the next step.


### Install

Install the package:

    $ pip install git+ssh://git@bitbucket.org/saao/filament.git

Then create a new application by running the following command, remembering to
change `<app_name>` to the name of your application:

    $ flask app create <app_name>

Enter the OAuth client ID and secret when prompted.


### Configure

Initialize the application database:

    $ flask db upgrade

The user database is initially empty, but you can add local user accounts using
the following command:

    $ flask user create <username>


### Dependencies

Server-side dependencies can be installed with the following command:

    $ python setup.py develop

Client-side dependencies are managed with Bower and built using Grunt:

    $ npm install -g bower
    $ npm install -g grunt-cli
    $ npm install

Note that these are Node-based tools that require the package manager
`npm` which is bundled with Node.

You're now ready to install the client-side dependencies:

    $ bower install

When making changes to any files inside of `filament/static/src` you will
need to rebuild these resources for your changes to take effect:

    $ grunt build


### Run

You can run your application with the following command:

    $ flask run


## For Developers

Your application makes use of the frameworks listed below:

- [Flask](http://flask.pocoo.org)
- [Node](https://nodejs.org)
- [Bower](https://bower.io)
- [Grunt](https://gruntjs.com)
- [Backbone](http://backbonejs.org)
- [Bootstrap](https://getbootstrap.com)
