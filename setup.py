from setuptools import setup, find_packages


setup(
    name='filament',
    version='1.0.0',
    description='User interface toolkit',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'blinker>=1.4',  # required by flask signals
        'cookiecutter>=1.6.0',
        'Flask>=1.0.2',
        'Flask-Assets>=0.12',
        'Flask-Dance>=1.0.0',
        'Flask-Login>=0.4.1',
        'Flask-Migrate==2.1.1',
        'Flask-SQLAlchemy>=2.3.2',
        'Flask-WTF>=0.14.3',
        'gunicorn>=19.9.0',
        'SQLAlchemy>=1.2.8',
        'SQLAlchemy-Utils>=0.33.3',
        'python-dotenv>=0.8.2',
        'requests>=2.18.4',
    ],
    entry_points={
        'flask.commands': [
            'app=filament.cli:app',
            'user=filament.cli:user',
        ],
    },
)
