import logging

from flask import Flask
from flask_assets import Bundle
from filament import portal, assets


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile('settings.py')

    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)

    portal.init_app(app)

    assets.register('js', Bundle(
        'filament/js/filament.js',
        output='js/app.js'
    ))

    assets.register('css', Bundle(
        'filament/css/filament.css',
        output='css/app.css'
    ))

    from app import views
    app.register_blueprint(views.main)
    app.register_blueprint(views.api, url_prefix='/api')

    return app
