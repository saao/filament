from flask import Blueprint, redirect, render_template, url_for
from flask_login import current_user, login_required


main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
@login_required
def index():
    return render_template('index.html')


api = Blueprint('api', __name__)
