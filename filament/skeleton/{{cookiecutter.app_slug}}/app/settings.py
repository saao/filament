import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = '{{ cookiecutter.secret_key }}'
DEBUG = False
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False
PORTAL_LOGIN_ENABLED = True
PORTAL_CLIENT_ID = '{{ cookiecutter.portal_client_id }}'
PORTAL_CLIENT_SECRET = '{{ cookiecutter.portal_client_secret }}'
PORTAL_BASE_URL = 'https://portal.saao.ac.za/'
PORTAL_TOKEN_URL = 'https://portal.saao.ac.za/o/token/'
PORTAL_AUTORIZATION_URL = 'https://portal.saao.ac.za/o/authorize/'
