from flask_dance.consumer import OAuth2ConsumerBlueprint
from flask_dance.consumer.storage.sqla import SQLAlchemyStorage
from flask_assets import Environment
from flask_login import LoginManager, current_user
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


class Portal(OAuth2ConsumerBlueprint):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):

        assets.init_app(app)
        login.init_app(app)
        db.init_app(app)
        migrate.init_app(app, db)

        super(Portal, self).__init__(
            'portal', __name__,
            client_id=app.config['PORTAL_CLIENT_ID'],
            client_secret=app.config['PORTAL_CLIENT_SECRET'],
            base_url=app.config['PORTAL_BASE_URL'],
            token_url=app.config['PORTAL_TOKEN_URL'],
            authorization_url=app.config['PORTAL_AUTORIZATION_URL'],
        )

        app.register_blueprint(self, url_prefix='/login')
        app.register_blueprint(filament)
        app.register_blueprint(auth)


assets = Environment()
login = LoginManager()
login.login_view = 'auth.login'
db = SQLAlchemy()
migrate = Migrate()
portal = Portal()


from filament.views import filament, auth
from filament.models import User, OAuth


portal.backend = SQLAlchemyStorage(OAuth, db.session, user=current_user)
