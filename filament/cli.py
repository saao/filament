import os
import binascii
import click

from getpass import getpass
from cookiecutter.main import cookiecutter
from flask.cli import AppGroup
from filament import db
from filament.models import User


app = AppGroup('app')


@app.command('create', with_appcontext=False)
@click.argument('app_name')
def create_app(app_name):
    """Create a new app"""

    cookiecutter(os.path.join(os.path.dirname(__file__), 'skeleton'),
        extra_context={
            'app_name': app_name,
            'secret_key': binascii.hexlify(os.urandom(64)),
        })


user = AppGroup('user')


@user.command('create')
@click.argument('username')
def create_user(username):
    """Create a new user"""

    user = User(username=username)
    user.set_password(getpass('Please enter a password: '))
    db.session.add(user)
    db.session.commit()
