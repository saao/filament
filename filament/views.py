import six
import sys
import traceback
import requests

from socket import timeout
from werkzeug.urls import url_parse
from sqlalchemy.orm.exc import NoResultFound
from flask import Blueprint, current_app, jsonify, render_template, request, redirect, url_for, flash
from flask_dance.consumer import oauth_authorized
from flask_login import current_user, login_user, logout_user, login_required
from filament import db
from filament.forms import LoginForm
from filament.models import User, OAuth


filament = Blueprint('filament', __name__,
    url_prefix='/filament',
    template_folder='templates',
    static_folder='static',
)


@filament.app_errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@filament.app_errorhandler(500)
def internal_error(error):
    if 'application/json' in list(request.accept_mimetypes):
        return jsonify({'error': error.message}), 500

    return render_template('500.html', error=error), 500


@filament.app_errorhandler(timeout)
def timeout_error(error):
    if 'application/json' in list(request.accept_mimetypes):
        if current_app.config.get('DEBUG', False):
            return jsonify({'debug': traceback.format_exc()}), 500
        else:
            raise Exception('Your request timed out. Please try again.')
    return render_template(
        '500.html', error='Your request timed out.',
        traceback=traceback.format_exc()), 500


@filament.app_errorhandler(Exception)
def error(error):
    """Returns the traceback for ajax requests when in debug mode."""
    if ('application/json' in list(request.accept_mimetypes) and
            current_app.config.get('DEBUG', False)):
        return jsonify({'debug': traceback.format_exc()}), 500
    six.reraise(*sys.exc_info())


@oauth_authorized.connect
def logged_in(portal, token):
    if not token:
        flash('Failed to log in with observer portal.', category='error')
        return False

    response = portal.session.get('/api/me')
    if not response.ok:
        flash('Failed to fetch user info from observer portal.', category='error')
        return False

    info = response.json()
    user_id = str(info['id'])

    # Find this OAuth token in the database, or create it
    query = OAuth.query.filter_by(
        provider=portal.name,
        provider_user_id=user_id,
    )

    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(
            provider=portal.name,
            provider_user_id=user_id,
            token=token,
        )

    if oauth.user:
        login_user(oauth.user)
        flash('Successfully logged in with observer portal.')
    else:
        # Create a new local user account for this user
        user = User(
            # Remember that `email` can be None.
            email=info['email'],
            username=info['username'],
        )

        # Associate the new local user account with the OAuth token
        oauth.user = user

        # Save and commit our database models
        db.session.add_all([user, oauth])
        db.session.commit()

        # Log in the new local user account
        login_user(user)
        flash('Successfully logged in with observer portal.')

    # Disable Flask-Dance's default behavior for saving the OAuth token
    return False


auth = Blueprint('auth', __name__, url_prefix='/auth')


@auth.route('/portal')
def portal():
    try:
        requests.head(current_app.config['PORTAL_BASE_URL'],
            headers={'User-Agent': 'Filament'})
    except Exception as e:
        flash('Failed to log in with observer portal.', category='error')
    else:
        return redirect(url_for('portal.login'))
    return redirect(url_for('.login'))


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for(
            current_app.config.get('LOGGED_IN_VIEW', 'main.index')))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password', category='error')
            return redirect(url_for('.login'))

        login_user(user)
        next_page = request.args.get('next')

        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for(
                current_app.config.get('LOGGED_IN_VIEW', 'main.index'))
        return redirect(next_page)
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for(
        current_app.config.get('LOGGED_IN_VIEW', 'main.index')))
