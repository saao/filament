module.exports = function(grunt) {
    const sass = require('node-sass');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                implementation: sass,
                outputStyle: 'compressed'
            },
            dist: {
                files: {
                    './filament/static/css/filament.css': './filament/static/src/scss/filament.scss'
                }
            }
        },

        concat: {
            dist: {
                files: {
                    './filament/static/js/filament.js': [
                        './bower_components/jquery/dist/jquery.js',
                        './bower_components/underscore/underscore.js',
                        './bower_components/backbone/backbone.js',
                        './bower_components/bootstrap/dist/js/bootstrap.bundle.js',
                    ]
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    './filament/static/js/filament.js': './filament/static/js/filament.js'
                }
            }
        },

        watch: {
            sass: {
                options: {
                    livereload: true
                },
                files: ['./filament/static/src/scss/*.scss'],
                tasks: ['sass']
            },
            js: {
                options: {
                    livereload: true
                },
                files: [
                    './filament/static/src/js/*.js',
                ],
                tasks: ['concat']
            },
            html: {
                options: {
                    livereload: true
                },
                files: ['./filament/templates/**/*.html']
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['sass', 'concat', 'uglify']);
}
